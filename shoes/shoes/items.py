# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ShoesItem(scrapy.Item):
    """ Shoes item model

        id: Unique item identifier.
        store: Store name
        name: Shoe comercial name
        price: Price in USD
        preview: Image from de product list
        categoy: Cantegory in the store
        create_time: product downloaded timestamp
        
    """
    id = scrapy.Field()
    store = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    preview = scrapy.Field()
    category = scrapy.Field()
    create_time = scrapy.Field()

