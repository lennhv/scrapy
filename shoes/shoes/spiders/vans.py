# -*- coding: utf-8 -*-
import scrapy
import datetime
import re, json
from ..items import ShoesItem


begin_index=re.compile(r'beginIndex\=\d+$')

class VansSpider(scrapy.Spider):
    """ Spider to download product data (shoes) from vans.com

    """

    name = 'vans'
    allowed_domains = ['vans.com']
    root_url='https://www.vans.com/shop'
    xpatterns={
        'catalog': '//span[@class="ajax-product-search-js hide"]',
        'item': '//div[@class="product-block product-block-js "]',
        'product_name': './/span[@class="product-block-name-wrapper"]/text()',
        'product_price': './/span[@class="product-block-price product-block-offer-price offer-price product-price-amount-js"]/@data-amount',
        'product_image': './/picture[@class="product-block-views-container views-container-js"]//img[@class="product-block-views-selected-view-main-image product-block-views-selected-view-image product-views-selected-view-main-image main-view-js"]/@srcset'
    }
    products_per_page=24
    stop_crawler=False
    categories = {
        'mens-shoes': 0,
        'womens-shoes': 0,
        'kids-girls-shoes': 0,
        'kids-little-boys-shoes': 0,
        'kids-toddler-baby-shoes': 0,
        'infant-baby-shoes': 0,
    }
    
    def __init__(self, category=None, *args, **kwargs):
        super(VansSpider, self).__init__(*args, **kwargs)
        if category:
            self.categories={category: 0}
        self.start_urls=[ (self.root_url + '/' + category, category) \
                            for category in self.categories
        ]
    
    def start_requests(self):
        for url, category in self.start_urls:
            self.logger.info("Start request {}".format(url) )
            request=scrapy.Request(url=url, callback=self.parse)
            request.meta['category'] = category
            yield request

    def parse(self, response):
        category=response.meta['category']
        self.logger.debug('Response from {} {}'\
                            .format(category, response.url) ) 
        # search pagination link to retrieve more items
        catalog_path=response.xpath(self.xpatterns['catalog']+'/@data-value').get()
        self.logger.debug("catalog path: {}".format(catalog_path) )
        if self.stop_crawler:
            self.logger.info("No more items. Stop crawling")
            return
        if catalog_path:
            match=begin_index.search(catalog_path).group()
            if match:
                index=match.split('=')
                index[-1] = str(int(index[-1]) + self.products_per_page)
                catalog_path = catalog_path.replace(match, '='.join(index))
                request=scrapy.Request(url='{}/{}'.format(self.root_url, catalog_path),
                                        callback=self.parse)
                request.meta['category'] = category
                yield request
        items=response.xpath(self.xpatterns['item'])
        if len(items)==0:
            # No items found, we assuming that no more products exists to retrieve
            self.stop_crawler = True
        for item in items:
            self.categories[category] += 1 
            product = ShoesItem(create_time=datetime.datetime.now())
            product['id'] = item.xpath('@data-part-number').get()
            product['store'] = self.root_url
            product['category'] = category
            product['name'] = item.xpath(self.xpatterns['product_name']).get()
            product['price'] = item.xpath(self.xpatterns['product_price']).get()
            product['preview'] = item.xpath(self.xpatterns['product_image']).get()
            if product['preview']:
                product['preview'] = 'https' + product['preview']
            yield product
