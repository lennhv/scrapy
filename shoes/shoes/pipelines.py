# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://dlatest/topics/item-pipeline.html

import pymongo

class ShoesPipeline(object):
    def process_item(self, item, spider):
        return item
    
    def close_spider(self, spider):
        spider.logger.info("data crawled: {}".format(spider.categories) )

class CsvWriterPipeline(object):
    """ Persist items into a csv file

        path_file: File to save date
        delimiters: Delimiters for csv format
    """
    
    def __init__(self, path_file, delimiter):
        self.path_file = path_file
        self.delimiter = delimiter
    
    @classmethod
    def from_crawler(cls, crawler):
        conf=crawler.settings.get('CSV', {})
        if not conf:
            raise ValueError('CSV configuration not found')
        return cls(
            path_file=conf.get('file'),
            delimiter=conf.get('delimiter', ';')
        )

    def open_spider(self, spider):
        self.file = open(self.path_file, 'w')
        line=[
            'id',
            'name',
            'price',
            'preview',
            'category',
            'store',
            'create_time',
        ]
        self.file.write(self.delimiter.join(line) + '\n')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line=[
            item['id'],
            item['name'],
            item['price'],
            item['preview'],
            item['category'],
            item['store'],
            item['create_time'].strftime("%Y-%m-%d %H:%M:%S.%f"),
        ]
        self.file.write(self.delimiter.join(line) + '\n' )
        return item

class MongoPipeline(object):
    """Persist data in mongodb

    """

    def __init__(self, uri, db, collection):
        self.uri = uri
        self.db = db
        self.collection = collection

    @classmethod
    def from_crawler(cls, crawler):
        conf=crawler.settings.get('MONGO_DB', {})
        if not conf:
            raise ValueError('Mongo configuration not found')
        return cls(
            uri=conf.get('uri'),
            db=conf.get('db', 'crawler'),
            collection=conf.get('collection', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.uri)
        self.db = self.client[self.db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[self.collection].insert_one(dict(item))
        return item
