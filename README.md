# Crawler

Example of use of the scrappy library to make a spider website in order to download the products that are offered on a shoe sales page, once downloaded the products are stored in CSV file and in a Mongo database, depending of the configuration.


## Configuration
Once the repository is cloned, the CSV and mongo storage can be configured as shown below:

    # Mongodb configuration
    MONGO_DB = {
    'uri': 'mongodb://localhost:27017/',
    'db': 'shoes',
    'collection': 'shoes'
    }

    # csv persistence data configuration
    CSV = {
    'file': '/tmp/shoes.csv',
    'delimiter': '|'
    }
To enable or disable a storage type, only the corresponding pipelines item should be commented.

 - CsvWriterPipeline 
 - MongoPipeline

# Running Spider
To run the spider:

    $ scrapy crawl vans

Once finished you can find the products downloaded in the CSV or mongo depending your configuration


